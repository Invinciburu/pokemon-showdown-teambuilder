package com.example.pokemonshowdownteambuilder;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_TEAM_NAME = "com.example.pokemonshowdownteambuilder.TEAM_NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void createTeam(View view) {
        Intent intent = new Intent(this, TeamActivity.class);
        EditText editText = (EditText) findViewById(R.id.editText);
        String newTeam = editText.getText().toString();
        intent.putExtra(EXTRA_TEAM_NAME, newTeam);
        startActivity(intent);
    }
}
